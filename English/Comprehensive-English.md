# 英语系列资料

## 综合英语Ⅰ
### 教材
![](https://pic.imgdb.cn/item/630039d616f2c2beb191a33b.jpg) 

下载地址：    

https://www.123pan.com/s/aiv9-gTrG  
提取码:DJTU   

### 课后答案  

附教师用书，内含`课后答案`及众多讲解

![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/%E7%8E%B0%E4%BB%A3%E5%A4%A7%E5%AD%A6%E8%8B%B1%E8%AF%AD%E7%B2%BE%E8%AF%BB%E6%95%99%E5%B8%88%E7%94%A8%E4%B9%A61.png) 

下载地址：https://kohler.lanzouv.com/iEx8y09zxrxi

## 综合英语Ⅱ

### 教材
![](https://pic.imgdb.cn/item/630039e716f2c2beb191aa10.jpg) 

下载地址：   
https://www.123pan.com/s/aiv9-YTrG  
提取码:DJTU

### 课后答案

附教师用书，内含`课后答案`及众多讲解

![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/%E7%8E%B0%E4%BB%A3%E5%A4%A7%E5%AD%A6%E8%8B%B1%E8%AF%AD%E7%B2%BE%E8%AF%BB%E6%95%99%E5%B8%88%E7%94%A8%E4%B9%A62.png) 



下载地址：https://kohler.lanzouv.com/izJhZ09zxv1a

## 综合英语Ⅲ

### 教材

![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/%E7%8E%B0%E4%BB%A3%E5%A4%A7%E5%AD%A6%E8%8B%B1%E8%AF%AD%E7%B2%BE%E8%AF%BB3.png) 



下载地址：https://kohler.lanzouv.com/i9M3L09zw7hg   

### 课后答案

附教师用书，内含`课后答案`及众多讲解

![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/%E7%8E%B0%E4%BB%A3%E5%A4%A7%E5%AD%A6%E8%8B%B1%E8%AF%AD%E7%B2%BE%E8%AF%BB%E6%95%99%E5%B8%88%E7%94%A8%E4%B9%A63.png) 



下载地址：https://kohler.lanzouv.com/iZPGV09zy05e

## 综合英语Ⅳ

### 教材

![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/%E7%8E%B0%E4%BB%A3%E5%A4%A7%E5%AD%A6%E8%8B%B1%E8%AF%AD%E7%B2%BE%E8%AF%BB4.png) 

下载地址：https://www.123pan.com/s/aiv9-VvrG  

提取码:DJTU

### 课后答案

附教师用书，内含`课后答案`及众多讲解

![1661132910093](https://gitee.com/kohler19/DJTUCourses/raw/master/img/1661132910093.jpg) 

下载：https://kohler.lanzouv.com/i2A2C09zvleb

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/social-share.js/1.0.16/css/share.min.css">

<div class="social-share"></div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/social-share.js/1.0.16/js/social-share.min.js"></script>
