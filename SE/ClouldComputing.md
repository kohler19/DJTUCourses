## 云计算(大型软件系统设计与体系结构)

### 课堂PPT

<img src="https://gitee.com/kohler19/DJTUCourses/raw/master/img/%E4%BA%91%E8%AE%A1%E7%AE%97.png" style="zoom: 67%;" />

下载（失效公众号直接回复）：https://kohler.lanzouo.com/itqzn0v6khdi

<img src="https://gitee.com/kohler19/DJTUCourses/raw/master/img/%E4%BA%91%E8%AE%A1%E7%AE%97book.png" alt="img" style="zoom: 33%;" />

下载：https://kohler.lanzouo.com/i7bVl0vg0wgb

### 笔记（THL整理）

<img src="https://gitee.com/kohler19/DJTUCourses/raw/master/img/%E4%BA%91%E8%AE%A1%E7%AE%97%E7%AC%94%E8%AE%B0thl.png" alt="img" style="zoom: 67%;" />

下载：https://kohler.lanzouo.com/iSlWD0vsj9mf

### 习题总结（7#楼446寝室奉献）

<img src="https://gitee.com/kohler19/DJTUCourses/raw/master/img/%E4%BA%91%E8%AE%A1%E7%AE%97%E4%B9%A0%E9%A2%98.png" alt="img" style="zoom:67%;" />

下载：https://kohler.lanzouo.com/iH2aQ0vsl0ja

### 头歌Docker基础

#### Docker基础实战教程一：入门

第1关 Hello Docker

```
docker pull busybox:latest
docker run --name first_docker_container busybox:latest echo "Hello Docker"
```

第2关 拉取镜像

```
docker pull busybox:1.27
```

第3关 启动一个容器

```#创建并启动一个容器，容器名为firstContainer，具备busybox的运行环境。并输出hello world
#拉取busybox最新镜像

docker run --name 'firstContainer' busybox echo "hello world"
```

第4关 停止一个容器

```
docker stop firstContainer
```

第5关 进入一个容器

``` 
docker exec container2 touch 1.txt
```

第6关 删除容器

``` #删除所有容器
#删除所有容器
docker rm -f $(docker ps -aq)
```

#### Docker基础实战教程二：镜像管理

第1关 基于Commit定制镜像

``` 
#将对容器container1做出的修改提交为一个新镜像，镜像名为busybox:v1
docker commit container1 busybox:v1
```

第2关 基于save保存镜像与基于load加载镜像

``` 
#!/bin/bash
#首先拉取一个busybox镜像
docker pull busybox:latest

#1.将busybox:latest镜像保存到tar包
#********** Begin *********#
docker save busybox:latest > alpine.tar
#********** End **********#

#删除busybox:latest镜像
docker rmi busybox:latest

#2.从tar包加载busybox:latest镜像
#********** Begin *********#
docker load < alpine.tar
#********** End **********#
```

第3关 导入导出容器

``` 
#1.然后将busyboxContainer导出为容器快照：busybox.tar
#********** Begin *********#
docker export busyboxContainer > busybox.tar
#********** End **********#

#2.最后使用该容器快照导入镜像，镜像名为busybox:v1.0。
#********** Begin *********#
cat busybox.tar | docker import - busybox:v1.0
#********** End **********#
```

第4关 删除镜像

``` 
#然后将busybox:latest镜像删除
#********** Begin *********#
docker rm container3
docker rmi busybox
#********** End **********#
```

第5关 构建私有Registry

``` 
#!/bin/bash
#构建一个私人仓库
docker pull registry:2
docker run -d -p 5000:5000 --restart=always --name myregistry registry:2

#拉取busybox镜像
docker pull busybox

#1.使用docker tag给busybox加上一个标签localhost:5000/my-busybox:latest
#********** Begin *********#
docker tag busybox:latest localhost:5000/my-busybox:latest
#********** End **********#

#2.将localhost:5000/my-busybox:latest镜像推送到私人仓库
#********** Begin *********#
docker push localhost:5000/my-busybox:latest
#********** End **********#

#删除本地镜像
docker rmi localhost:5000/my-busybox:latest

#3.从私人仓库拉取localhost:5000/my-busybox:latest镜像
#********** Begin *********#
docker pull localhost:5000/my-busybox:latest
#********** End **********#

#删除私人仓库并将私人仓库中的镜像也删除掉
docker rm -vf myregistry
```

#### Docker基础实战教程三：Dockerfile

第1关 初识Dockerfile

``` 
#创建一个空文件夹，并进入其中
mkdir newdir1
cd newdir1
#创建一个Dockerfile文件
touch Dockerfile
#假设我的Dockerfile文件为
#FROM ubuntu
#RUN mkdir dir1
#可以这么写：
# echo 'FROM ubuntu' > Dockerfile
# echo 'RUN mkdir dir1'>> Dockerfile
#输入Dockerfile文件内容
#********** Begin *********#
#以busybox为基础镜像

#在基础镜像的基础上，创建一个hello.txt文件
echo 'FROM busybox' > Dockerfile
echo 'RUN touch hello.txt' >>  Dockerfile
#********** End **********#
#使用Dockerfile创建一个新镜像，镜像名为busybox:v1
docker build -t busybox:v1 .
```

第2关 docker build、COPY和ADD

``` 
#创建一个空文件夹，并进入其中
mkdir newdir2
cd newdir2
#创建一个文件夹dir1，将其压缩，然后删除dir1
mkdir dir1 && tar -cvf dir1.tar dir1 && rmdir dir1
#创建一个Dockerfile文件
touch Dockerfile
#假设我的Dockerfile文件为
#FROM ubuntu
#RUN mkdir dir1
#可以这么写：
# echo 'FROM ubuntu' > Dockerfile
# echo 'RUN mkdir dir1'>> Dockerfile
#输入Dockerfile文件内容
#********** Begin *********#
echo 'FROM busybox' > Dockerfile
#并将上下文目录下的dir1.tar“解压提取后”，拷贝到busybox:v3的/
echo 'ADD dir1.tar /' >>  Dockerfile
#********** End **********#

#文件内容完毕，在当前文件夹中执行
#********** Begin *********#
#以该Dockerfile构建一个名为busybox:v3的镜像
docker build -t busybox:v3 .
#********** End **********#
```

第3关 CMD和ENTRYPOINT指令

``` 
#创建一个空文件夹，并进入其中
mkdir newdir3
cd newdir3
#创建一个Dockerfile文件
touch Dockerfile
#假设我的Dockerfile文件为
#FROM ubuntu
#RUN mkdir dir1
#可以这么写：
# echo 'FROM ubuntu' > Dockerfile
# echo 'RUN mkdir dir1'>> Dockerfile
#输入Dockerfile文件内容
#********** Begin *********#
#以busybox为基础镜像
echo 'FROM busybox' > Dockerfile
echo 'ENTRYPOINT ["df"]'>> Dockerfile
echo 'CMD ["-Th"]'>> Dockerfile
#********** End **********#

#文件内容完毕，在当前文件夹中执行
#********** Begin *********#
#以该Dockerfile构建一个名为mydisk:latest的镜像
docker build -t mydisk:latest .
#********** End **********#
```

第4关 ENV、EXPOSE、WORKDIR、ARG指令

``` 
#创建一个空文件夹，并进入其中
mkdir newdir4
cd newdir4
#创建一个Dockerfile文件
touch Dockerfile
#假设我的Dockerfile文件为
#FROM ubuntu
#RUN mkdir dir1
#可以这么写：
# echo 'FROM ubuntu' > Dockerfile
# echo 'RUN mkdir dir1'>> Dockerfile
#输入Dockerfile文件内容
#********** Begin *********#
#以busybox为基础镜像
echo 'FROM busybox' > Dockerfile
#声明暴露3000端口
echo 'EXPOSE 3000' >>Dockerfile
#将变量var1=test设置为环境变量
echo 'ENV var1=test '>>Dockerfile
#设置工作目录为/tmp
echo 'WORKDIR /tmp'>>Dockerfile
#在工作目录下创建一个1.txt文件
echo "RUN touch 1.txt" >> Dockerfile
#********** End **********#
#文件内容完毕，在当前文件夹中执行
#********** Begin *********#
#以该Dockerfile构建一个名为testimage:v1的镜像
docker build -t testimage:v1 .
#********** End **********#
```

第5关 ONBUILD和VOLUME指令

``` 
#无论任何输入都会认为正确，本关的目的是学习ONBUILD和VOLUME指令
ONBUILD指令
●ONBUILD添加一个将来执行的触发器(trigger)；
格式： ONBUILD <其它指令>；

ONBUILD 是一个特殊的指令，它后面跟的是其它指令，比如RUN， COPY等，而这些指令，在当前镜像构建时并不会被执行。只有当以当前镜像为基础镜像，去构建下一级镜像的时候才会被执行。

ONBUILD指令的具体执行步骤
（1）在构建过程中，ONBUILD指令会添加到触发器指令镜像元数据中，这些触发器指令并不会在当前构建过程中执行。
（2）在构建过程后，触发器指令会被存储在镜像详情中，其主键是OnBuild，可以使用docker inspect命令查看。
（3）在之后该镜像可能作为其他Dockerfile中FROM指令的参数。在构建过程中，FROM指令会查找ONBUILD触发器指令，并且会以它们注册的顺序执行。若有触发器指令执行失败，则FROM指令被中止，并返回失败；若所有触发器指令执行成功，则FROM指令完成并继续执行下面的指令。在镜像构建完成后，触发器指令会被清除，不会被子孙镜像继承。

ONBUILD指令的实例
1.首先编写一个Dockerfile文件，内容如下所示：

FROM busybox
ONBUILD RUN touch 1.txt
2.利用上面的Dockerfile文件构建一个新镜像：docker build -t image1 .。执行docker run image1 cat 1.txt，提示：cat: can't open '1.txt': No such file or directory。可以知道基于image1镜像构建的容器中不存在1.txt文件。我们通过inspect image1，在里面可以找到。

"OnBuild":[
    "RUN touch 1.txt"
],
3.编写一个新的Dockerfile文件，内容如下所示：

FROM image1
RUN echo 'hello'
4.利用第三步创建的的Dockerfile文件构建一个新镜像:docker build -t image2 .。如下所示：在执行完FROM指令后，首先执行的是触发器，也就是# Executing 1 build trigger...，该指令创建了一个1.txt文件。然后才执行RUN echo 'hello'。执行docker run image2 cat 1.txt，执行成功！！

[root@localhost dir1]# docker build -t image2 .
Sending build context to Docker daemon 2.048 kB
Step 1/2 : FROM image1
# Executing 1 build trigger...
Step 1/1 : RUN touch 1.txt
 ---> Running in 5c9b99ef7801
 ---> 4c5b719176b8
Removing intermediate container 5c9b99ef7801
Step 2/2 : RUN echo 'hello'
 ---> Running in 8a2598e90e1f
hello
 ---> f5642bb4a975
Removing intermediate container 8a2598e90e1f
Successfully built f5642bb4a975
[root@localhost dir1]# docker run image2 cat 1.txt`
[root@localhost dir1]#
VOLUME指令
●VOLUME定义匿名卷；
格式：VOLUME ["<路径1>", "<路径2>"...]或VOLUME <路径>；

之前我们说过，容器运行时应该尽量保持容器存储层不发生写操作，对于数据库类需要保存动态数据的应用，其数据库文件应该保存于数据卷(volume)中，后面的章节我们会进一步介绍Docker数据卷的概念。
```

#### Docker基础实战教程四：数据卷操作

第1关 创建一个数据卷

```
#!/bin/bash
#创建一个名为vo1的数据卷，并将该数据卷挂载到container1容器的/dir1目录。
#拉取ubutun 最新镜像，实际生产中，docker pull 这一步可以省略，docker run的时候会自己去拉取。
docker pull ubuntu

#********** Begin *********#
docker run -v vo1:/dir1 --name container1 ubuntu
#********** End **********#
```

第2关 挂载和共享数据卷

```
#1.创建一个名为container1的容器，并将本地主机的/dir1目录挂载到容器中的/codir1中。
#拉取ubutun 最新镜像，实际生产中，docker pull 这一步可以省略，docker run的时候会自己去拉取。
docker pull ubuntu

#********** Begin *********#
docker run -v /dir1:/codir1 --name container1 ubuntu
#********** End **********#
#2.创建一个名为container2的容器，与container1共享数据卷。
#********** Begin *********#
docker run --volumes-from container1 --name container2 ubuntu
#********** End **********#
```

第3关 查看数据卷的信息

``` 
#创建一个容器，并创建一个随机名字的数据卷挂载到容器的/data目录
#拉取ubutun 最新镜像，实际生产中，docker pull 这一步可以省略，docker run的时候会自己去拉取。
docker pull ubuntu &> /dev/null 
docker rm container1 -f &>/dev/null
docker run -v /data --name container1 ubuntu
#输出容器container1创建的数据卷的名字
#********** Begin *********#
docker inspect --type container --format='{{range .Mounts}}{{.Name}}{{end}}' container1
#********** End **********#
```

第4关 删除数据卷

``` 
#!/bin/bash
#创建一个名为container1的容器，创建一个数据卷挂载到容器的/data目录
#拉取ubutun 最新镜像，实际生产中，docker pull 这一步可以省略，docker run的时候会自己去拉取。
docker pull ubuntu

docker run -v vo4:/data --name container1 ubuntu
#删除container1对应的数据卷
#********** Begin *********#
docker rm -v container1
docker volume rm vo4
#********** End **********#
```

第5关 备份、恢复数据卷

```
#!/bin/bash
#拉取ubutun 最新镜像，实际生产中，docker pull 这一步可以省略，docker run的时候会自己去拉取。
docker pull ubuntu
# 创建一个vo1的数据卷，并在数据卷中添加1.txt文件
docker run --name vocontainer1 -v vo1:/dir1 ubuntu touch /dir1/1.txt
#1.将vo1数据卷的数据备份到宿主机的/newback中,将容器的/backup路径挂载上去，并将容器内/dir1文件夹打包至/backup/backup.tar
#********** Begin *********#
docker run --volumes-from vocontainer1 -v /newback:/backup  ubuntu tar -cvf /backup/backup.tar /dir1
#********** End **********#
#删除所有的容器以及它使用的数据卷
docker rm -vf $(docker ps -aq)
docker volume rm vo1
#在次创建一个vo1的数据卷
docker run -itd --name vocontainer2 -v vo1:/dir1 ubuntu /bin/bash
#2.将保存在宿主机中备份文件的数据恢复到vocontainer2的/中
#********** Begin *********#
docker run --volumes-from vocontainer2 -v /newback:/backup ubuntu tar -xvf /backup/backup.tar -C /
#********** End **********#
```

