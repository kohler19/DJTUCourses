# 离散数学

## 课本

英文原版   
![](https://pic.imgdb.cn/item/62ff8bcb16f2c2beb1514e25.jpg)   
下载：https://www.123pan.com/s/aiv9-7zDG

汉语版   
![](https://pic.imgdb.cn/item/62ff8bfe16f2c2beb1516330.jpg)   

下载：https://www.123pan.com/s/aiv9-bzDG   

## 课堂PPT  

![](https://pic.imgdb.cn/item/62ff8c5016f2c2beb1518417.jpg)   

下载：https://kohler.lanzouv.com/iIF3309urirc

## 慕课

慕课推荐北交大的刘铎老师的慕课     

<iframe src="//player.bilibili.com/player.html?aid=93412237&bvid=BV1ME41147Md&cid=159470052&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>