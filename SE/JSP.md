## JSP（基于WEB程序设计）

### 一些可能需要的教程

JDK的安装和配置：https://www.cnblogs.com/kohler21/p/16778482.html

idea 的Tomcat 的简单配置：https://www.cnblogs.com/kohler21/p/16778626.html

### 教材电子版

<img src="https://gitee.com/kohler19/DJTUCourses/raw/master/img/JSP%E6%95%99%E6%9D%90.png" style="zoom: 25%;" />

下载地址：https://kohler.lanzouy.com/iIorf0g6mmeb

### PPT

![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/JSPPPT.png)

蓝奏云（不限速）：https://kohler.lanzouy.com/iUSiE17g6gna

### 课本配套源代码

JSP 实用教程第4版-微课版源代码与习题解答：

- https://kohler.lanzouy.com/iyBQ417g6dsh
- https://pan.baidu.com/s/1W0mNdUKgEUaVKcbRB1lVtw?pwd=jsp4

### 课后题答案

https://kohler.lanzouy.com/igjt617g6pub

### 配套网课

<iframe src="//player.bilibili.com/player.html?aid=714276474&bvid=BV1XX4y157zx&cid=300337442&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>