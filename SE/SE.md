# 软件工程专业资料

## 软件工程

### 课堂PPT（唐斌版）

![](https://pic.imgdb.cn/item/62fdb3cf16f2c2beb11c9059.jpg)   

下载：  
https://kohler.lanzouv.com/iDOXH09qyccj

### 课本

英文版课本：  
![](https://pic.imgdb.cn/item/62fdbbe716f2c2beb1217307.jpg)   

下载：  
https://www.123pan.com/s/aiv9-ZyDG

汉语版课本：  
![](https://pic.imgdb.cn/item/62fdbc2b16f2c2beb12190b9.jpg)    

下载：   
https://www.123pan.com/s/aiv9-byDG

课堂笔记总结：
![](https://pic.imgdb.cn/item/62fde1dd16f2c2beb1368b31.jpg)   
   
https://kohler.lanzouv.com/izXaH09rcz4d

课后习题解答：     
https://www.123pan.com/s/aiv9-cyDG


