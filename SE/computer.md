#  计算机科学导论

## 课本

![](https://pic.imgdb.cn/item/62ff863016f2c2beb14de4c9.jpg)   

下载：https://www.123pan.com/s/aiv9-0TrG

## 课堂PPT

![](https://pic.imgdb.cn/item/62ff86a416f2c2beb14e239e.jpg)   

下载：https://kohler.lanzouv.com/iTlBA09upvcd

## 习题（包含题库）

下载：https://kohler.lanzouv.com/irydR09upyti

## 慕课资源

慕课资源推荐西北大学的计算机科学导论课程

<iframe src="//player.bilibili.com/player.html?aid=840497726&bvid=BV1S54y1Q718&cid=184647540&page=2" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>