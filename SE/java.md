# 面向对象编程(java)
本课程使用的教材是本校老师耿祥义教授编著，推荐大家关注耿祥义老师公众号：`java-violin`

耿祥义老师公众号资源汇总：[耿祥义主要教材资源汇总](https://mp.weixin.qq.com/s/-fkGOM5T_mhzZdW5P0PKxQ)
## 课本
![](https://pic.imgdb.cn/item/630218ee16f2c2beb17156fd.jpg)    

下载：https://www.123pan.com/s/aiv9-6vrG   
提取码:DJTU

## 习题答案

![](https://pic.imgdb.cn/item/630219bb16f2c2beb171ac20.jpg)       

下载：  
https://kohler.lanzouv.com/ilQeU09z37oj

## 课件及代码

![](https://pic.imgdb.cn/item/63021b3316f2c2beb1724355.jpg)    
下载：https://kohler.lanzouv.com/icFbA09z3pib   

## java题库

![](https://pic.imgdb.cn/item/63021bc916f2c2beb17288c8.jpg)

使用方法：
1. 双击文件夹下的`runApp.bat`   
![](https://pic.imgdb.cn/item/63021c4e16f2c2beb172ccf6.jpg)        
然后会出现:    
![](https://pic.imgdb.cn/item/63021c7c16f2c2beb172e567.jpg)      
然后随便按键盘上一个键进入   
会出现：   
![](https://pic.imgdb.cn/item/63021cb416f2c2beb1730c55.jpg)   

然后输入密码：  `gxyjava2syjc`  
即可进入题库。
## 配套视频教程   
课本配套教程：   
<iframe src="//player.bilibili.com/player.html?aid=667676946&bvid=BV1Sa4y1t7o3&cid=176600538&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>

## 期末复习视频教程

<iframe src="//player.bilibili.com/player.html?aid=419096080&bvid=BV11V411W7RD&cid=367438963&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>

## 课设相关视频

<iframe src="//player.bilibili.com/player.html?aid=376396297&bvid=BV1Jo4y1C7As&cid=362211921&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>
