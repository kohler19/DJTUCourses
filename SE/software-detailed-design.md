## 简介
这个课基本上就是`软件工程`课程，很多都一样不过更为详细了，教材什么都还是一样，但是这个课的教材版本用的软件工程第10版，软件工程课程用的第8版。最后是要做一个项目的设计文档的，还有考试，考试背题就完了，但是那个项目的文档还是挺费劲的。
## 课件

![软件详细设计1.png](https://s2.loli.net/2023/06/26/KlhELtY938bgvZU.png)

下载：    

下载地址1：[软件工程详细设计课件](https://kohler.lanzoup.com/ikYc910h3did)  (点击下载)

下载地址2：[软件工程详细设计课件](https://www.writebug.com/git/kohler/SEsource/raw/branch/main/%E8%BD%AF%E4%BB%B6%E5%B7%A5%E7%A8%8B%E8%AF%A6%E7%BB%86%E8%AE%BE%E8%AE%A1/%E8%BD%AF%E4%BB%B6%E5%B7%A5%E7%A8%8B%E8%AF%A6%E7%BB%86%E8%AE%BE%E8%AE%A1%E8%AF%BE%E4%BB%B6.zip)(点击下载)

课件合集下载：
下载地址1：[课件合集](https://kohler.lanzoup.com/iEEiU10hhs7g)
下载地址2：[课件合集](https://www.writebug.com/document/fdacb50a-140f-11ee-8c98-0242ac1e000f)