## 软件项目管理案例教程
### 课堂PPT：   
![](https://pic.imgdb.cn/item/62fde2e116f2c2beb1371f91.jpg)   
下载：      
https://kohler.lanzouv.com/iCcsj09rdcvi   

### 课本：  
![](https://pic.imgdb.cn/item/62fde39816f2c2beb137bc6e.jpg)   

下载链接1：https://kohler.lanzouh.com/ibsmY0568a7c   
下载链接2：https://www.123pan.com/s/aiv9-b6DG

上机实践会用到的画图软件：   
https://kohler.lanzouv.com/izxS1032caud      

课后习题答案：   
https://kohler.lanzouv.com/i3Z8K09rdtuj  
