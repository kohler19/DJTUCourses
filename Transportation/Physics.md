## 课本

![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/%E5%A4%A7%E5%AD%A6%E7%89%A9%E7%90%86%E4%B8%8A%E8%AF%BE%E6%9C%AC.png) 



下载：https://kohler.lanzouv.com/ivtFP0a191na

## 课堂PPT

PPT为ch1-ch14   

下载：https://kohler.lanzouv.com/iqWje0a16l0j

## 作业及答案

![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/%E5%A4%A7%E7%89%A9%E4%BD%9C%E4%B8%9A%E5%8F%8A%E7%AD%94%E6%A1%88.png) 

下载：https://kohler.lanzouv.com/iC65k0a169eb

## 大物总结

![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/%E5%A4%A7%E7%89%A9%E6%80%BB%E7%BB%93.png) 

下载：https://kohler.lanzouv.com/i1uDM0a16pgj

## 大物2019考试大纲（仅供参考）

下载：https://kohler.lanzouv.com/iw5dA0a17bbg



## 慕课推荐

推荐东北大学-马文蔚老师的课程

<iframe src="//player.bilibili.com/player.html?aid=19087994&bvid=BV1qW411H7UX&cid=31136694&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>

该课程配套课件：https://kohler.lanzouv.com/i7YuZ0a18rtg