<!-- _coverpage.md -->

<!--![](https://pic.imgdb.cn/item/629ff8c809475431299428df.png)-->

# 大连交通大学课程共享<small>by 愷龍</small>

> 来到一所大学，从第一次接触许多课，直到一门一门完成，这个过程中我们时常收集起许多资料和情报。

- 我希望能够将这些隐晦的、不确定的、口口相传的资料和经验，变为公开的、易于获取的和大家能够共同完善、积累的共享资料。
- 我希望只要是前人走过的弯路，后人就不必再走。
- 这是我的信念，也是我建立这个项目的原因。

[Gitee](https://gitee.com/kohler19/DJTUCourses)
[Get Started](https://kohler19.gitee.io/djtucourses/#/home\guide)