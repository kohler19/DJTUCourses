# 大连交通大学课程共享

## 使用方法
手机端演示：  
![](https://s1.ax1x.com/2022/08/17/vBzXND.jpg)   
     
手机访问页面点击左下角三个横线图标即可看到目录   
 
![](https://s1.ax1x.com/2022/08/17/vD9cnI.jpg)    

根据目录来查找所需要的资料
且可以通过搜索框快速搜索所需要的资料

## 建议与反馈
由于资料均为个人收集，难免存在错误或者缺失的部分，如果您有好的建议或者有反馈意见可以通过邮箱（lklong2001@qq.com）向我反馈，或者扫描下面的二维码（或点击链接）向我反馈。同时如果您有相关资料愿意分享的话也欢迎您的分享，同样可以通过邮箱向我联系。
同时您也可以通过微信公众号向我咨询和反馈建议与意见，欢迎您的咨询，我会及时回复的。   
反馈意见或建议：     
![](https://pic.imgdb.cn/item/63006cad16f2c2beb1ab05e5.jpg)    
链接直达：https://www.wjx.top/vm/wApKkPG.aspx    
公众号：  
![](https://s1.ax1x.com/2022/08/17/vDP1y9.png)

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/social-share.js/1.0.16/css/share.min.css">
<div class="social-share"></div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/social-share.js/1.0.16/js/social-share.min.js"></script>

