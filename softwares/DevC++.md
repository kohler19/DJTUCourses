## DevC++的安装教程  
- [dev-c++ 使用教程](https://gitee.com/kohler19/DJTUCourses/blob/master/softwares/DevC++%E6%95%99%E7%A8%8B.md)    
Dev C++ 是一款免费开源的 C/C++ IDE，内嵌 GCC 编译器（GCC 编译器的 Windows 移植版），是 NOI、NOIP 等比赛的指定工具。Dev C++ 的优点是体积小（只有几十兆）、安装卸载方便、学习成本低，缺点是调试功能弱。    
安装 Dev C++ 跟安装普通软件一样，远没有安装 VS 那么复杂。   
Dev C++ 5.11 简体中文版下载地址：   
官方下载：https://sourceforge.net/projects/orwelldevcpp/   
百度网盘：https://pan.baidu.com/s/1mhHDjO8    提取密码：mken   
火墙之内，有时候无法访问 SourceForge 这个网站，建议大家去百度网盘下载。  
开始安装 Dev C++  
Dev C++ 下载完成后会得到一个安装包（.exe 程序），双击该文件即可开始安装。  

1) 首先加载安装程序（只需要几十秒）  

![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/1F42QO8-0.png)  
2) 开始安装  

![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/1F42W028-1.png)  
Dev C++ 支持多国语言，包括简体中文，但是要等到安装完成以后才能设置，在安装过程中不能使用简体中文，所以这里我们选择英文（English）。  
 
3) 同意 Dev C++ 的各项条款   
![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/1F42R3H-2.png)  

4) 选择要安装的组件  
![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/1F42T016-3.png)   
选择“Full”，全部安装。  

5) 选择安装路径  
![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/1F42S540-4.png)  
你可以将 Dev C++ 安装在任意位置，但是路径中最好不要包含中文。  

6) 等待安装  
![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/1F42W960-5.png)  

7) 安装完成  
![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/1F42Sc7-6.png)  
开始配置 Dev C++
首次使用 Dev C++ 还需要简单的配置，包括设置语言、字体、和主题风格。  

1) 第一次启动 Dev C++ 后，提示选择语言。  
![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/1F42U910-7.png)  
这里我们选择简体中文，英语给力的朋友也可以选择英文。  

2) 选择字体和主题风格  
![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/1F42RD9-8.png)   
这里保持默认即可。  

3) 提示设置成功   
![](https://gitee.com/kohler19/DJTUCourses/raw/master/img/1F42R0X-9.png)
点击“OK”按钮，进入 Dev C++，就可以编写代码了。  