## dev-c++ 使用教程 
- [DevC++安装教程](https://gitee.com/kohler19/DJTUCourses/blob/master/softwares/DevC++.md)

Dev C++ 支持单个源文件的编译，如果你的程序只有一个源文件（初学者基本都是在单个源文件下编写代码），那么不用创建项目，直接运行就可以；如果有多个源文件，才需要创建项目。

#### 一、新建源文件

##### 1、通过步骤点击新建源文件

打开 Dev C++，在上方菜单栏中选择`文件 --> 新建 --> 源代码`

![](https://s2.loli.net/2023/07/04/zNxmB6gwTRtLnsi.png)

##### 2、通过快捷键方式新建源文件

按下`Ctrl+N`组合键，都会新建一个空白的源文件

![](https://s2.loli.net/2023/07/04/Ir7oemK5uzpdfOj.png)

#### 二、在文件中输入案例的代码

##### 1、输入源代码

```C
#include <stdio.h> //固定抬头 
int main(){
	printf("hello,world!");
	return 0;
}
```

![](https://s2.loli.net/2023/07/04/MPoc2iIAudBgah3.png)

##### 2、保存源代码

在上方菜单栏中选择“文件 --> 保存”，或者按下`Ctrl+S`组合键，都可以保存源文件。

<img src="https://s2.loli.net/2023/07/04/vXn8AMtcoQjwVDW.png" style="zoom:67%;" />

**注：** dev-c++ 默认的源代码格式是`c++`，但是C++是在C语言的基础上进行的扩展，C++已经包含了C语言的全部内容，所以大部分 IDE 默认创建的是C++文件，但是这并不影响使用，**但是一般使用什么语言就设置什么格式的文本**。

#### 三、生成可执行程序

##### 1、选择按钮编译源代码

在上方菜单栏中选择`编译`按钮，就可以完成 `hello.c` 源文件的编译工作。

![](https://s2.loli.net/2023/07/04/9KcvW3a6USFPHVB.png)

##### 2、选择快捷键`F9`编译源代码

##### 3、编译结果

如果代码没有错误，会在下方的“编译日志”窗口中看到编译成功的提示：

![](https://s2.loli.net/2023/07/04/VHAFyMbtOodYBQa.png)

##### 4、查看运行结果

编译完成后，打开源文件所在的目录，会看到多了一个名为`hello.exe`的文件，这就是最终生成的可执行文件。

![](https://s2.loli.net/2023/07/04/xOdkzWc8R7bjuqN.png)

双击`hello.exe`文件即可查看运行结果，但是一闪而过，看不见，所以需要添加语句，是的可查看运行结果。

```c
#include <stdio.h> //固定抬头 
#include <stdlib.h>
int main(){
	printf("hello,world!");
	system("pause");
	return 0;
}
```

[<img src="https://s1.ax1x.com/2023/07/04/pCs0Uv6.png" alt="pCs0Uv6.png" style="zoom:67%;" />](https://imgse.com/i/pCs0Uv6)

- **注：** 代码开头部分还添加了`#include <stdlib.h>`语句，否则`system("pause");`无效。
- 再次编译，查看运行结果即可。

<img src="https://s2.loli.net/2023/07/04/sJtoa5jIBM6dn8D.png"/>

#### 四、更加便捷运行的方式

实际开发中我们一般点击“编译运行”按钮，或者直接按下`F11`键：

![](https://s2.loli.net/2023/07/04/Vh8udWJ9jp3x6Mi.png)

这样做的一个好处是，编译器会让程序自动暂停，我们也不用再添加`system("pause");`语句了。

```c
#include <stdio.h> //固定抬头 
int main(){
	printf("hello,world!");
	return 0;
}
```

![](https://s2.loli.net/2023/07/04/wzq4C6sIJAiBbeG.png)