## 常用网站
| PDF处理类 | 图片处理类 | 教程类 | 实用工具类 | PPT类 |
|--------|-------|-----|-------|------|
| PDF派：https://www.pdfpai.com/|快速AI自动抠图：https://www.remove.bg/zh   |     |       |      |
|        |       |     |       |      |
|        |       |     |       |      |

## 编程常用软件
### Java
- idea [idea 的理论永久使用教程](https://kohler19.gitee.io/djtucourses/pages/idea-2023-pj.html)

### C语言
- [DevC++的安装教程](https://kohler19.gitee.io/djtucourses/pages/DevC++install.html)
- [Dev-c++ 使用教程](https://kohler19.gitee.io/djtucourses/pages/devc++use.html)